package game.peanutpanda.jam9.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class WaitingScreen extends BaseScreen {

    public Animation<TextureRegion> clock;
    float timePassed;

    public WaitingScreen(Game game) {
        super(game);

        clock = createAnim("clock");
    }

    @Override
    public void show() {
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0 / 255f, 0 / 255f, 0 / 255f, 1);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        stage.getBatch().setProjectionMatrix(camera.combined);

        drawAnim(clock, 250, 250, 300, 300, false);

        stage.act();
        stage.draw();

        timePassed += delta * 2;

        if (timePassed > 0.75f) {
            choiceTime += delta;
        }

        if (timePassed > 10) {
            choiceTime = 0;
            reInitLabel("You ignore the knocking and wait for it to go away. The seconds seem to last forever, you start to lose track of time and space.\n\n" +
                    "Are you hallucinating, is this even real? Your mind is playing tricks on you. You don't feel like yourself anymore.\n\n" +
                    "The walls are bleeding now. You watch the blood trickle down from the walls onto the floor.\n\n" +
                    "You are unsure if something has been triggered, but suddenly the door swings wide open.", 320);
            addChoices("Walk out the door", "Drink the blood");
        }


        if (choice2Chosen) {
            dispose();
            game.setScreen(new DrinkBloodScreen(game));
        }

        if (choice1Chosen) {
            dispose();
            game.setScreen(new WalkOutScreen(game));
        }


    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
