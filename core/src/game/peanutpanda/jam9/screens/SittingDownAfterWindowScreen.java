package game.peanutpanda.jam9.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class SittingDownAfterWindowScreen extends BaseScreen {

    public Animation<TextureRegion> sittingAnim;
    float timePassed;

    public SittingDownAfterWindowScreen(Game game) {
        super(game);

        sittingAnim = createAnim("sitting");
    }

    @Override
    public void show() {
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0 / 255f, 0 / 255f, 0 / 255f, 1);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        stage.getBatch().setProjectionMatrix(camera.combined);

        drawAnim(sittingAnim, 250, 250, 300, 300, false);

        stage.act();
        stage.draw();

        timePassed += delta * 2;

        if (timePassed > 0.75f) {
            choiceTime += delta * 1.3f;
        }


        if (timePassed > 10) {
            choiceTime = 0;
            reInitLabel("You sit down on the only chair in the room to gather your thoughts. You try to remember how you got here but your mind is blank.\n\n" +
                    "While you're thinking about whatever it is you just saw outside, you hear a sound...\n\n\n" +
                    "Someone is knocking on the door.", 350);
            addChoices("Open the door", "Wait it out");
        }

        if (choice1Chosen) {
            dispose();
            game.setScreen(new OpenDoorAfterWindow(game));
        }

        if (choice2Chosen) {
            dispose();
            game.setScreen(new WaitingScreen(game));
        }


    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
