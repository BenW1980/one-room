package game.peanutpanda.jam9.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class WalkOutScreen extends BaseScreen {

    public Animation<TextureRegion> walkOut;
    float timePassed;

    public WalkOutScreen(Game game) {
        super(game);

        walkOut = createAnim("walkOut");
    }

    @Override
    public void show() {
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0 / 255f, 0 / 255f, 0 / 255f, 1);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        stage.getBatch().setProjectionMatrix(camera.combined);

        drawAnim(walkOut, 250, 250, 300, 300, false);

        stage.act();
        stage.draw();

        timePassed += delta * 2;

        if (timePassed > 0.75f) {
            choiceTime += delta;
        }

        if (timePassed > 10) {
            choiceTime = 0;
            reInitLabel("You walk out the door and leave the room behind you forever. You never found out how you got there or what the purpose of the room was.\n\n" +
                    "Although you escaped, the room has never really left you. There was always a feeling of unease you were never able to get rid of, " +
                    "a feeling of always being watched.\n\n" +
                    "You live out the rest of your days in mediocrity until you finally die of old age, alone and decrepit.", 300);
            addGameOverButton(false, this);
        }


//        if (choice1Chosen) {
//            game.setScreen(new SittingDownAfterDoorScreen(game));
//        }


    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
