package game.peanutpanda.jam9.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.rafaskoberg.gdx.typinglabel.TypingLabel;
import game.peanutpanda.jam9.AssetLoader;
import game.peanutpanda.jam9.ScreenSize;

public class BaseScreen implements Screen {

    public Game game;
    public AssetLoader assetLoader;
    public Skin skin;
    public OrthographicCamera camera;
    public Viewport viewport;
    public Stage stage;
    public float choiceTime, animTime;
    public Table choicesTable, rootTable;
    public TypingLabel label;
    public boolean labelAdded;

    public boolean choicesAdded, choice1Chosen, choice2Chosen;

    public BaseScreen(Game game) {

        this.game = game;

        assetLoader = new AssetLoader();
        assetLoader.loadSkins();
        assetLoader.manager.finishLoading();
        skin = assetLoader.skin();

        this.camera = new OrthographicCamera();
        this.camera.setToOrtho(false, ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize());
        this.viewport = new FitViewport(ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize(), camera);
        this.stage = new Stage(viewport);

        rootTable = new Table(skin);
        rootTable.setFillParent(true);

//        reInitLabel("");

        choicesTable = new Table(skin);
        rootTable.add(choicesTable);
        stage.addActor(rootTable);
//        stage.addActor(label);

        resetChoicesTable();

        Gdx.input.setInputProcessor(stage);
        System.out.println("refe");

    }

    @Override
    public void show() {

    }

    protected void resetChoicesTable() {
        choicesAdded = false;
        choicesTable.clear();
    }

    protected void reInitLabel(String text, float y) {

        if (!labelAdded) {
            labelAdded = true;
            label = new TypingLabel("{SPEED=1.1}" + text, skin);
            label.setColor(Color.YELLOW);
            label.setWrap(true);
            label.setSize(600, 125);
            label.setPosition(140, y);
            stage.addActor(label);
        }
    }

    protected void startGame() {
        TextButton textButton1 = new TextButton("Enter the room", skin, "title");
        textButton1.addListener((new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new FirstScreen(game));

            }
        }));
        textButton1.getLabel().setAlignment(Align.center);

        choicesTable.add(textButton1).padTop(450).width(100).align(Align.center);
    }


    protected void newGame(Screen screen) {

        if (label.hasEnded() && !choicesAdded) {
            choicesTable.clear();

            choiceTime = 0;
            choice1Chosen = false;
            choice2Chosen = false;
            TextButton textButton1 = new TextButton("Enter the room and try again", skin);
            textButton1.addListener((new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    label.remove();
                    labelAdded = false;
                    screen.dispose();
                    game.setScreen(new FirstScreen(game));

                }
            }));
            textButton1.getLabel().setAlignment(Align.left);

            choicesTable.add(textButton1).padTop(450).width(600).align(Align.center);
            choicesAdded = true;
        }
    }

    protected void addGameOverButton(boolean win, Screen screen) {

        if (label.hasEnded() && !choicesAdded) {
            choicesTable.clear();

            choiceTime = 0;
            choice1Chosen = false;
            choice2Chosen = false;
            TextButton textButton1 = new TextButton("Continue", skin);
            textButton1.addListener((new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    label.remove();
                    labelAdded = false;
                    screen.dispose();
                    game.setScreen(new GameOverScreen(game, win));

                }
            }));
            textButton1.getLabel().setAlignment(Align.left);

            choicesTable.add(textButton1).padTop(450).width(600).align(Align.center);
            choicesAdded = true;
        }
    }


    protected void addChoices(String text1, String text2) {

        if (label.hasEnded() && !choicesAdded) {
            choicesTable.clear();

            choiceTime = 0;
            choice1Chosen = false;
            choice2Chosen = false;
            TextButton textButton1 = new TextButton(text1, skin);
            textButton1.addListener((new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    label.remove();
                    labelAdded = false;
                    choice1Chosen = true;

                }
            }));

            TextButton textButton2 = new TextButton(text2, skin);
            textButton2.addListener((new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    label.remove();
                    labelAdded = false;
                    choice2Chosen = true;

                }
            }));
            textButton1.getLabel().setAlignment(Align.left);
            textButton2.getLabel().setAlignment(Align.left);

            choicesTable.add(textButton1).padTop(450).width(600).align(Align.center);
            choicesTable.row();
            choicesTable.add(textButton2).padTop(5).width(600).align(Align.center);

            choicesAdded = true;
        }
    }

    protected Animation<TextureRegion> createAnim(String name) {
        TextureAtlas smileyAtlas = assetLoader.manager.get("anim/" + name + ".atlas", TextureAtlas.class);
        return new Animation<>(0.1f, smileyAtlas.getRegions());
    }

    protected void drawAnim(Animation<TextureRegion> anim, float x, float y, float width, float height, boolean looping) {

        stage.getBatch().begin();
        stage.getBatch().draw(anim.getKeyFrame(choiceTime, looping), x, y, width, height);
        stage.getBatch().end();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0 / 255f, 0 / 255f, 0 / 255f, 1);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        delta = Math.min(delta, 0.1f);

        camera.update();
        stage.getBatch().setProjectionMatrix(camera.combined);

        stage.act();
        stage.draw();


    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    @Override
    public void dispose() {
        assetLoader.manager.dispose();
        stage.dispose();
    }
}
