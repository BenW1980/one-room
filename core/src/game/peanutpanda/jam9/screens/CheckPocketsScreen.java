package game.peanutpanda.jam9.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class CheckPocketsScreen extends BaseScreen {

    public Animation<TextureRegion> family;
    float timePassed;

    public CheckPocketsScreen(Game game) {
        super(game);

        family = createAnim("family");
    }

    @Override
    public void show() {
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0 / 255f, 0 / 255f, 0 / 255f, 1);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        stage.getBatch().setProjectionMatrix(camera.combined);

        drawAnim(family, 250, 250, 300, 300, false);

        stage.act();
        stage.draw();

        timePassed += delta * 2;

        if (timePassed > 0.75f) {
            choiceTime += delta;
        }

        if (timePassed > 10) {
            choiceTime = 0;
            reInitLabel("As you try to compose yourself, you feel something in your shirt pocket.\n\n" +
                    "You take it out, it's a picture of a family of four. You wonder how it ended up in your pocket.\n\n" +
                    "", 400);
            addChoices("Rip the picture in half", "Put the picture back on the table");
        }


        if (choice1Chosen) {
            dispose();
            game.setScreen(new PictureRippedScreen(game));
        }
        if (choice2Chosen) {
            dispose();
            game.setScreen(new PutPictureBackScreen(game));
        }

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
