package game.peanutpanda.jam9.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import game.peanutpanda.jam9.ScreenSize;

public class SecondScreen extends BaseScreen {

    public Animation<TextureRegion> roomAnim;
    float timePassed;

    public SecondScreen(Game game) {
        super(game);

        roomAnim = createAnim("room");
    }

    @Override
    public void show() {
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0 / 255f, 0 / 255f, 0 / 255f, 1);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        stage.getBatch().setProjectionMatrix(camera.combined);

        drawAnim(roomAnim, 0, 0, ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize(), false);

        stage.act();
        stage.draw();

        timePassed += delta * 2;

        if (timePassed > 0.75f) {
            choiceTime += delta * 2;
        }

        if (timePassed > 10) {
            choiceTime = 0;
            reInitLabel("You are standing in the middle of an unremarkable room. You don't recognize it and you don't remember how you got here.\n" +
                    "\nYou have a slight headache, you feel as if you could use some fresh air.", 400);
            addChoices("Open the door", "Open the window");
        }

        if (choice2Chosen) {
            dispose();
            game.setScreen(new LookWindowScreen(game));
        }

        if (choice1Chosen) {
            dispose();
            game.setScreen(new OpenDoorScreen(game));
        }

        System.out.println(choicesAdded + "-" + choice1Chosen + "-" + choice2Chosen);


    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
