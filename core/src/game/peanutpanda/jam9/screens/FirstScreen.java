package game.peanutpanda.jam9.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class FirstScreen extends BaseScreen {

    public Animation<TextureRegion> roomAnim;

    public FirstScreen(Game game) {
        super(game);
        roomAnim = createAnim("room");
        reInitLabel("You find yourself in a room.", 450);
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0 / 255f, 0 / 255f, 0 / 255f, 1);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        stage.getBatch().setProjectionMatrix(camera.combined);

        stage.act();
        stage.draw();

        addChoices("Look around", "Do nothing");

        if (choice1Chosen) {
            dispose();
            game.setScreen(new SecondScreen(game));
        }

        if (choice2Chosen) {
            choice2Chosen = false;
            reInitLabel("You still find yourself in a room.", 450);
        }

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
