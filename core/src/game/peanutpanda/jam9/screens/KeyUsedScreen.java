package game.peanutpanda.jam9.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class KeyUsedScreen extends BaseScreen {

    public Animation<TextureRegion> parchment;
    float timePassed;

    public KeyUsedScreen(Game game) {
        super(game);

        parchment = createAnim("parchment");
    }

    @Override
    public void show() {
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0 / 255f, 0 / 255f, 0 / 255f, 1);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        stage.getBatch().setProjectionMatrix(camera.combined);

        drawAnim(parchment, 250, 250, 300, 300, false);

        stage.act();
        stage.draw();

        timePassed += delta * 2;

        if (timePassed > 0.75f) {
            choiceTime += delta * 1.3f;
        }


        if (timePassed > 15) {
            choiceTime = 0;
            reInitLabel("You put the key in the lock and turn it. You feel as if something is blocking the key, as if there is someone on the other side that " +
                    "is not allowing you to leave.\n\n" +
                    "A piece of parchment slides in from under the door. As you read the ominous words, the key breaks.\n\n" +
                    "The only way out is now closed forever. You live out the remainder of your days within the confines of the room.", 320);
            addGameOverButton(false,this);
        }

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
