package game.peanutpanda.jam9.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class LookWindowScreen extends BaseScreen {

    public Animation<TextureRegion> windowAnim;
    float timePassed;

    public LookWindowScreen(Game game) {
        super(game);

        windowAnim = createAnim("window");
    }

    @Override
    public void show() {
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0 / 255f, 0 / 255f, 0 / 255f, 1);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        stage.getBatch().setProjectionMatrix(camera.combined);

        drawAnim(windowAnim, 250, 250, 300, 300, false);

        stage.act();
        stage.draw();

        timePassed += delta * 2;

        if (timePassed > 0.75f) {
            choiceTime += delta;
        }

        if (timePassed > 10) {
            choiceTime = 0;
            reInitLabel("As you attempt to open the window, you notice there is a figure standing outside. You can't make out any details but it seems that it's looking" +
                    " right at you. \n\nYou take a step back and refrain from opening the window.", 400);
            addChoices("Sit down for a moment", "Look at the table");
        }


        if (choice1Chosen) {
            dispose();
            game.setScreen(new SittingDownAfterWindowScreen(game));
        }

        if (choice2Chosen) {
            dispose();
            game.setScreen(new LookingAtTableScreen(game));
        }

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
