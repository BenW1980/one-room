package game.peanutpanda.jam9.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.rafaskoberg.gdx.typinglabel.TypingLabel;

public class StartupScreen extends BaseScreen {

    public Table rootTable;

    public Label titleLabel, readyLabel;
    public TypingLabel typingLabel;

    public Animation<TextureRegion> loadingAnimation;

    public StartupScreen(Game game) {

        super(game);

        loadingAnimation = createAnim("loading");

        rootTable = new Table(skin);
        rootTable.background("window");
        rootTable.setFillParent(true);

        titleLabel = new Label("**** COMMODORE 64 BASIC V2 ****\n\n64K RAM SYSTEM  38911 BASIC BYTES FREE", skin);
        titleLabel.setAlignment(Align.center);
        readyLabel = new Label("READY.", skin);
        readyLabel.setAlignment(Align.left);
        typingLabel = new TypingLabel("{SPEED=0.3}LOAD \"LIBGDX_JAM_AUGUST_2019\"{SPEED=10}\n\n" +
                "PRESS PLAY ON TAPE\n{WAIT=1}LOADING...\nREADY\nRUN.{WAIT=0.75} ", skin);

        rootTable.add(titleLabel).top().expandY().padTop(10);
        rootTable.row();
        rootTable.add(readyLabel).left().expandY().expandX().padTop(30);
        rootTable.row();
        rootTable.add(typingLabel).left().expandY().expandX().padBottom(305).width(500);

        stage.addActor(rootTable);
//        stage.setDebugAll(true);
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0 / 255f, 0 / 255f, 0 / 255f, 1);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        delta = Math.min(delta, 0.1f);

        camera.update();
        stage.getBatch().setProjectionMatrix(camera.combined);

        stage.act();
        stage.draw();


        stage.getBatch().begin();
        if (typingLabel.hasEnded()) {
            choiceTime += delta * 2;
//            drawAnim(loadingAnimation, 0, 0, ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize(), true);
        }

        if (choiceTime > 1) {
            dispose();
            game.setScreen(new FirstScreen(game));
        }
        stage.getBatch().end();

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
