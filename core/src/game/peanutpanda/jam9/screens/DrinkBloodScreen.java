package game.peanutpanda.jam9.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class DrinkBloodScreen extends BaseScreen {

    public Animation<TextureRegion> transformation;
    float timePassed;

    public DrinkBloodScreen(Game game) {
        super(game);

        transformation = createAnim("transformation");

    }

    @Override
    public void show() {
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0 / 255f, 0 / 255f, 0 / 255f, 1);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        stage.getBatch().setProjectionMatrix(camera.combined);

        drawAnim(transformation, 250, 250, 300, 300, false);

        stage.act();
        stage.draw();

        timePassed += delta * 2;

        if (timePassed > 0.75f) {
            choiceTime += delta;
        }

        if (timePassed > 10) {
            choiceTime = 0;
            reInitLabel("As you drink the blood, you slowly start to forget who you once were. The more you drink, the harder it becomes to remember.\n\n" +
                    "The blood invigorates every fibre of your being.\n\n" +
                    "As the old you is dying, a different you is emerging. One that can only think of consuming. A pure and beautiful being, lusting for blood.\n\n" +
                    "You have achieved what few others before you have achieved. You have fulfilled your true potential, you were destined to become this." +
                    " A beast of darkness and " +
                    "instinct.", 300);
            addGameOverButton(true,this);
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
