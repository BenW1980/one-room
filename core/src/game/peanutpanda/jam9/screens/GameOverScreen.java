package game.peanutpanda.jam9.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;

public class GameOverScreen extends BaseScreen {

    float timePassed;
    boolean fulfilledPotential;

    public GameOverScreen(Game game, boolean fulfilledPotential) {
        super(game);
        this.fulfilledPotential = fulfilledPotential;

    }

    @Override
    public void show() {
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0 / 255f, 0 / 255f, 0 / 255f, 1);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        stage.getBatch().setProjectionMatrix(camera.combined);


        stage.act();
        stage.draw();


        choiceTime = 0;
        if (fulfilledPotential) {
            reInitLabel("You have reached the end.\n\n\n\n" +
                    "Congratulations. You have fulfilled your true potential.", 350);
            newGame(this);
        } else {
            reInitLabel("You have reached the end.\n\n\n\n" +
                    "Sadly you have not fulfilled your true potential. You have failed.", 350);
            newGame(this);
        }


    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
