package game.peanutpanda.jam9.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class SwallowedKeyScreen extends BaseScreen {

    public Animation<TextureRegion> cryAnim;
    float timePassed;

    public SwallowedKeyScreen(Game game) {
        super(game);

        cryAnim = createAnim("cry");
    }

    @Override
    public void show() {
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0 / 255f, 0 / 255f, 0 / 255f, 1);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        stage.getBatch().setProjectionMatrix(camera.combined);

        drawAnim(cryAnim, 250, 250, 300, 300, false);

        stage.act();
        stage.draw();

        timePassed += delta * 2;

        if (timePassed > 0.75f) {
            choiceTime += delta * 1.3f;
        }


        if (timePassed > 10) {
            choiceTime = 0;
            reInitLabel("You are not sure why, but you feel compelled to. As if you were being manipulated by a higher force.\n\n" +
                    "You put the small key in your mouth. As you feel it finding its way down your throat, you can't help but think about " +
                    "what it was for. It probably unlocked the door.\n\n\n" +
                    "You live out the remainder of your days within the confines of the room.", 320);
            addGameOverButton(false,this);
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
