package game.peanutpanda.jam9.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class LookingAtTableScreen extends BaseScreen {

    public Animation<TextureRegion> keyAnim;
    float timePassed;

    public LookingAtTableScreen(Game game) {
        super(game);

        keyAnim = createAnim("key");
    }

    @Override
    public void show() {
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0 / 255f, 0 / 255f, 0 / 255f, 1);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        stage.getBatch().setProjectionMatrix(camera.combined);

        drawAnim(keyAnim, 250, 250, 300, 300, false);

        stage.act();
        stage.draw();

        timePassed += delta * 2;

        if (timePassed > 0.75f) {
            choiceTime += delta * 1.3f;
        }


        if (timePassed > 10) {
            choiceTime = 0;
            reInitLabel("You notice a small key on the table, you pick it up.\n\n" +
                    "You wonder what it opens. Could it be the door?", 400);
            addChoices("Use the key on the door", "Swallow the key");
        }

        if (choice1Chosen) {
            dispose();
            game.setScreen(new KeyUsedScreen(game));
        }

        if (choice2Chosen) {
            dispose();
            game.setScreen(new SwallowedKeyScreen(game));
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
