package game.peanutpanda.jam9.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class OpenDoorScreen extends BaseScreen {

    public Animation<TextureRegion> doorAnim;
    float timePassed;

    public OpenDoorScreen(Game game) {
        super(game);

        doorAnim = createAnim("door");
    }

    @Override
    public void show() {
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0 / 255f, 0 / 255f, 0 / 255f, 1);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        stage.getBatch().setProjectionMatrix(camera.combined);

        drawAnim(doorAnim, 250, 250, 300, 300, false);

        stage.act();
        stage.draw();

        timePassed += delta * 2;

        if (timePassed > 0.75f) {
            choiceTime += delta;
        }

        if (timePassed > 10) {
            choiceTime = 0;
            reInitLabel("As you attempt to open the door, you faintly hear a scratching sound coming from the other side. As you move closer to the door, " +
                    "the noise becomes louder." +
                    " \n\nYou take a step back and refrain from opening the door.", 400);
            addChoices("Sit down for a moment", "Check your pockets");
        }


        if (choice1Chosen) {
            dispose();
            game.setScreen(new SittingDownAfterDoorScreen(game));
        }

        if (choice2Chosen) {
            dispose();
            game.setScreen(new CheckPocketsScreen(game));
        }


    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
