package game.peanutpanda.jam9.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class PictureRippedScreen extends BaseScreen {

    public Animation<TextureRegion> pictureBlood;
    float timePassed;

    public PictureRippedScreen(Game game) {
        super(game);

        pictureBlood = createAnim("pictureBlood");
    }

    @Override
    public void show() {
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0 / 255f, 0 / 255f, 0 / 255f, 1);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        stage.getBatch().setProjectionMatrix(camera.combined);

        drawAnim(pictureBlood, 250, 250, 300, 300, false);

        stage.act();
        stage.draw();

        timePassed += delta * 2;

        if (timePassed > 0.75f) {
            choiceTime += delta;
        }

        if (timePassed > 10) {
            choiceTime = 0;
            reInitLabel("As you rip up the picture, blood comes pouring out of it.\nIt runs over your hands and drips down onto the floor.\n\n" +
                    "You are unsure if your action has triggered something, but suddenly the door swings wide open. Maybe it was just the wind.", 400);
            addChoices("Drink the blood", "Walk out the door");
        }


        if (choice1Chosen) {
            dispose();
            game.setScreen(new DrinkBloodScreen(game));
        }

        if (choice2Chosen) {
            dispose();
            game.setScreen(new WalkOutScreen(game));
        }


    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
