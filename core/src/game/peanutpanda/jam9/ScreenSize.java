package game.peanutpanda.jam9;

public enum ScreenSize {

    WIDTH(800), HEIGHT(600);

    private final int size;

    ScreenSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

}