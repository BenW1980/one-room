package game.peanutpanda.jam9;

import com.badlogic.gdx.Game;
import game.peanutpanda.jam9.screens.TitleScreen;

public class Main extends Game {

    @Override
    public void create() {
        setScreen(new TitleScreen(this));
    }
}
