package game.peanutpanda.jam9;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.SkinLoader;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class AssetLoader {

    public final AssetManager manager = new AssetManager();

    public void loadSkins() {
        SkinLoader.SkinParameter params = new SkinLoader.SkinParameter("skin/uiskin.atlas");
        manager.load("skin/uiskin.json", Skin.class, params);

        manager.load("title.png", Texture.class);

        manager.load("sound/dark1.mp3", Music.class);

        manager.load("anim/loading.atlas", TextureAtlas.class);
        manager.load("anim/room.atlas", TextureAtlas.class);
        manager.load("anim/window.atlas", TextureAtlas.class);
        manager.load("anim/door.atlas", TextureAtlas.class);
        manager.load("anim/sitting.atlas", TextureAtlas.class);
        manager.load("anim/key.atlas", TextureAtlas.class);
        manager.load("anim/cry.atlas", TextureAtlas.class);
        manager.load("anim/alienWindow.atlas", TextureAtlas.class);
        manager.load("anim/alienDoor.atlas", TextureAtlas.class);
        manager.load("anim/clock.atlas", TextureAtlas.class);
        manager.load("anim/family.atlas", TextureAtlas.class);
        manager.load("anim/pictureBlood.atlas", TextureAtlas.class);
        manager.load("anim/walkOut.atlas", TextureAtlas.class);
        manager.load("anim/transformation.atlas", TextureAtlas.class);
        manager.load("anim/headBlood.atlas", TextureAtlas.class);
        manager.load("anim/parchment.atlas", TextureAtlas.class);
    }

    public Skin skin() {
        return manager.get("skin/uiskin.json", Skin.class);
    }

    public Texture title() {
        return manager.get("title.png", Texture.class);
    }

    public Music dark1() {
        return manager.get("sound/dark1.mp3", Music.class);
    }

}